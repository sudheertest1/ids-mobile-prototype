# IDS Prototype

IDS Prototype is a  application that demonstrates how to build mobile app with Ionic using leaflet mapping API. 

## Installation Instructions

Follow these instructions to install the app and run it:


1. Clone the repository:
    ```
    git clone https://github.com/lssinc/ids-mobile-prototype 
    ```

2. Navigate to the `ids-mobile-prototype ` directory :
    ```
    cd ids-mobile-prototype 
    ```
3. Install the dependencies
     ```
      npm install
     ```
  
4. Run the app in the browser
     ```
      ionic serve
     ```
5. For RICOH THETA Service calls, Please fallow the below instructions:
   1. Go to app --> environment-variables
   2. Open development.ts file
   3. Change CALL_RICOH_SERVICES value to "true".
   4. Change RICOH_CAMERA_URL value to IP where RICOH CAMERA is hosted
   5. Go to app  --> services 
   6. Open ricohtheta.service.ts
   7. Change the URL in all the methods to current IP where the Camera hosted. 
