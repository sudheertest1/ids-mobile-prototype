import { Component, ElementRef} from '@angular/core';
import { NavController } from 'ionic-angular';
import { HTTP } from '@ionic-native/http';
declare var $:any;

@Component({
    selector: 'page-about',
    templateUrl: 'about.html'
})
export class AboutPage {
    public thetaIP:string = 'http://192.168.1.1/';
    public thetaOSCPath:string = this.thetaIP +'osc/commands/execute';
    public session_id:string = null;//"SID_0001";//null;
    public session_expires:boolean = null;
    public thetaNetworkSSID:string = null;
    public connectedToTheta:boolean = false;
    public defaultHeaders:any = {
		'Content-Type' : 'application/json;',
		'X-XSRF-Protected': '1'
	};
    public thetaStatus:any = null;
    constructor(public navCtrl: NavController,private elRef:ElementRef,private http: HTTP) {

    }
    ngOnInit(): void {
        $(".panorama").panorama_viewer({
            repeat: true
        });

    }
    public takePicture(){
        this.startCameraSession();
        //this.takePhoto();
    }

    // public getInfo(){
    //      this.http.get(`https://conduit.productionready.io/api/profiles/eric`).map((res:Response) => res.json());
    // }
    public startCameraSession(){
        console.log('Attempt To Start Theta Session');
        var me = this;
        var params ={
    "name": "camera.startSession",
    "parameters": {}
  };
        console.log(me.thetaOSCPath);
        console.log(params);
        console.log(me.defaultHeaders);
        this.http.post(me.thetaOSCPath, params,{}).then(data => {
            console.log('Theta Session Started');
            me.session_id = data["sessionId"];
            console.log(data);
        }).catch(error => {

            console.log('Failed To Start Theta Session');
            //me.error('Failed To Start Theta Session', error);
            console.log(error);
        });
    }

    public takePhoto(){
        console.log('Attempt To Start Theta Session');
        var me = this;
        var params = {
            "name": "camera.takePicture",
            "parameters": {"sessionId": me.session_id}
        };
        // let postParams = {
        //     title: 'foo',
        //     body: 'bar',
        //     userId: 1
        // }
        //me.thetaOSCPath ="http://jsonplaceholder.typicode.com/posts";

        this.http.post(me.thetaOSCPath, params, me.defaultHeaders).then(data => {
            console.log('Photo Capture Triggered');
            // me.session_id = data.results.sessionId;
            console.log(data);
        }).catch(error => {

            console.log('Failed To Trigger Photo Capture');
            //me.error('Failed To Trigger Theta Photo Capture', error);
            console.log(error);
        });
    }

}
