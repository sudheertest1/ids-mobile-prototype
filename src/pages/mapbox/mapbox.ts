import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import * as mapboxgl from '../../../node_modules/mapbox-gl/dist/mapbox-gl.js';
import { Map } from '../../../node_modules/mapbox-gl/dist/mapbox-gl.js';
@Component({
    selector: 'page-mapbox',
    templateUrl: 'mapbox.html'
})
export class MapboxPage {
    map:any;
    constructor(public navCtrl: NavController) {
        (mapboxgl as any).accessToken = 'pk.eyJ1IjoicGF0cmlja3IiLCJhIjoiY2l2aW9lcXlvMDFqdTJvbGI2eXUwc2VjYSJ9.trTzsdDXD2lMJpTfCVsVuA';
    }
    ngOnInit(): void {
        let map = new Map({
            container: 'map',
            style: 'mapbox://styles/mapbox/satellite-v9',
            hash: false,
            zoom: 16,
            center: [ -71.23506724834444,41.75234492017432]
        });
        this.map = map;
        new mapboxgl.Marker().setLngLat([-71.23506724834444,41.75234492017432]).addTo(map);

        map.on('click', function (e) {
            console.log(e.lngLat);
        });
    }
}
