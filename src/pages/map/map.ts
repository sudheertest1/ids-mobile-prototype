import { Component,Inject} from '@angular/core';
import { NavController,ModalController, NavParams,AlertController  } from 'ionic-angular';
import { RicohThetaService } from '../../app/services/ricohtheta.service';
import { RicohPicture } from '../../app/services/ricohpicture';
import { ModelPage } from '../model/model';

import { envVariables } from "../../app/environment-variables/environment-variables.token";

import * as Leaflet from 'leaflet';
import '../../../node_modules/leaflet-plugins/layer/tile/Bing.js';

/*
 Generated class for the Map page.

 See http://ionicframework.com/docs/v2/components/#navigation for more info on
 Ionic pages and navigation.
 */
@Component({
  selector: 'page-map',
  templateUrl: 'map.html'
})
export class MapPage {
  public _this:any = this;
  public location_marked:boolean;
  public photo_location: any;
  public BING_KEY:string = this.envVariables.BING_KEY;
  public map: any;
  public baseLocation: any;
  public pointedLocation:any;
  public photos: RicohPicture;
  public ricohSessionObject: any;
  public callRicohService:boolean = this.envVariables.CALL_RICOH_SERVICES;
  public appliedBingLayer:boolean = false;
  public bingLayer:any;
  public raster2dLayer:any;
  public raster3dLayer:any;
  public appliedRaster2D:boolean = false;
  public appliedRaster3D:boolean = false;
  public locations: any[] = [{id:1,name:"Lone Oak - Eagan"},{id:2,name:"Holiday Inn Express - Swansea, MA"},{id:3,name:"Tidewater Beach Resort"}];
  public locationLngLats = {1:[44.846667,-93.156381],2:[41.752359,-71.235407],3:[30.220249,-85.886159]};
  public rasterDimentions = {1:[[44.846798542894035,-93.15654158592226],[44.846505684556796,-93.15616607666017]],
    2:[[41.752545020734324,-71.23578608036043],[41.752152823049855,-71.23497605323793]],
    3:[[30.22078201369915,-85.88683247566225],[30.21978077251238,-85.88553428649904]]};
  public selectedLocationId :number;
  constructor(public navCtrl: NavController,private ricohThetaService:RicohThetaService,public modalCtrl: ModalController,private navParams: NavParams, @Inject(envVariables) public envVariables,public alertCtrl: AlertController) {

  }

  ngOnInit(): void {
    this.photos = {name: '', state: "",results: {
      "fileUrl": ""
    } } ;
    //Creating a new Map with Leaflet
    this.map = new Leaflet.Map('map', {center: new Leaflet.LatLng(34.14053607,-118.22490378 ), zoom: 17 });
    //Add OSM layer
    var openStreetMapTileUrl = this.envVariables.OPEN_STREET_MAP_URL;
    var openStreet = new Leaflet.TileLayer(openStreetMapTileUrl);
    this.map.addLayer(openStreet);
    //Adding a market to the Map
    Leaflet.marker(this.locationLngLats[1]).addTo(this.map);
    Leaflet.marker(this.locationLngLats[2]).addTo(this.map);
    Leaflet.marker(this.locationLngLats[3]).addTo(this.map);

    //Event to find the location and set the marker on the Map
    this.map.on('locationfound', (e) => { this.onLocationFound(e); });
    this.map.on('locationerror', (e) => { this.onLocationError(e); });

    //Registering click event for the map.
    this.map.on('click', (e) => { this.setLocation(e); });
    //Calling a Mock or Richo Theta Service to Start a session
    debugger;
    if(this.callRicohService) {
      this.ricohThetaService.startSession().subscribe(data => this.ricohSessionObject = data,
          err => alert('Problem with Calling EndPoint'));
    }else {
      this.ricohSessionObject = this.ricohThetaService.startSessionMock();
    }
  }
  //Event handler to setting the current location
  public onLocationFound(e) {
    this.baseLocation = e.latlng;
    var radius = e.accuracy / 2;
    Leaflet.marker(e.latlng).addTo(this.map);
    Leaflet.circle(e.latlng, radius).addTo(this.map);
  }
  //Error handling of map loader
  public onLocationError  (e) {
    this.showAlert(e.message);
  }

  //Event handler to map click event , sets a mrker and dtore the Geo location lnglat for further use
  public setLocation (e){
    console.log(e.latlng.lat+","+e.latlng.lng);
    this.location_marked = true;
    this.pointedLocation = e.latlng;
    this.photo_location = e.latlng;
    Leaflet.marker(e.latlng).addTo(this.map).bindPopup(" "+e.latlng).openPopup();
  }

  //Event handler for the shooting button
  public takePicture() {
    //Calling a Mock or Richo Theta Service to take a picture
    if(this.callRicohService) {
      this.photos = this.ricohThetaService.takePhoto(this.pointedLocation,this.ricohSessionObject.results.sessionId).subscribe(data => this.photos = data,
          err => alert('Problem with Calling EndPoint'));
    }else {
      this.photos = this.ricohThetaService.takePhotoMock(this.pointedLocation, this.ricohSessionObject.results.sessionId);
    }
    if(!this.location_marked){
      alert("Please select a location first");
      return;
    }
    let modal = this.modalCtrl.create(ModelPage,{photo:this.photos.results["fileUrl"],lat:this.photo_location.lat,lng:this.photo_location.lng,name:this.photos.name,state:this.photos.state});
    modal.present();
  }
  //Event to add Bing layer
  public addBingLayer() {
    //Adding a Bing Layer to map
    if(this.appliedBingLayer) {
      this.map.removeLayer(this.bingLayer);
      this.appliedBingLayer = false;
    }else {
      var imagerySet = "AerialWithLabels"; // AerialWithLabels | Birdseye | BirdseyeWithLabels | Road
      this.bingLayer = new Leaflet.BingLayer(this.BING_KEY , {type: imagerySet,maxZoom:22,maxNativeZoom:19});
      this.map.addLayer(this.bingLayer);
      this.appliedBingLayer = true;
    }

  }

  //Event for adding 2D layer to Image
  public add2DRasterLayer() {
    if(!this.selectedLocationId) {
      var bounds = new Leaflet.LatLngBounds(
          new Leaflet.LatLng(34.140536, -118.224904),
          new Leaflet.LatLng(34.141690, -118.223099));
      //var map = Leaflet.map('map').fitBounds(bounds);
      this.map.fitBounds(bounds);
      var options = {
        minZoom: 17,
        maxZoom: 24,
        opacity: 1.0,
        tms: false
      };
      Leaflet.tileLayer('/assets/map_tiles/{z}/{x}/{y}.png', options).addTo(this.map);
      //this.showAlert("Please select Location");
      return;
    }
    if(this.appliedRaster2D) {
      this.map.removeLayer(this.raster2dLayer);
      this.appliedRaster2D = false;
    }else{
      var imageUrl = 'assets/images/layout-1.png',
          imageBounds = this.rasterDimentions[this.selectedLocationId];
      this.raster2dLayer = Leaflet.imageOverlay(imageUrl, imageBounds);
      this.map.addLayer(this.raster2dLayer);
      this.appliedRaster2D = true;
    }
  }
  public add3DRasterLayer() {
    if(!this.selectedLocationId) {
      this.showAlert("Please select Location");
      return;
    }
    if(this.appliedRaster3D) {
      this.map.removeLayer(this.raster3dLayer);
      this.appliedRaster3D = false;
    }else{
      var imageUrl = 'assets/images/layout-3d.jpg',
          imageBounds = this.rasterDimentions[this.selectedLocationId];
      this.raster3dLayer = Leaflet.imageOverlay(imageUrl, imageBounds);
      this.map.addLayer(this.raster3dLayer);
      this.appliedRaster3D = true;
    }
  }
  public gotoLocation(e) {
    if(!e.id){
      if(!this.selectedLocationId) {
        this.showAlert("Something went wrong!!!");
      }
    }
    var selectedLocationLatLng = this.locationLngLats[e.id];
    this.selectedLocationId = e.id;
    this.map.flyTo(new Leaflet.LatLng(selectedLocationLatLng[0],selectedLocationLatLng[1]),17,{animate:true});
    if(this.appliedRaster3D) {
      this.map.removeLayer(this.raster3dLayer);
      this.appliedRaster3D = false;
    }
    if(this.appliedRaster2D) {
      this.map.removeLayer(this.raster2dLayer);
      this.appliedRaster2D = false;
    }
  }

  showAlert(message) {
    let alert = this.alertCtrl.create({
      title: 'Error',
      subTitle: message,
      buttons: ['OK']
    });
    alert.present();
  }

}
