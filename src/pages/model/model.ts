import { Component } from '@angular/core';
import { NavController,NavParams  } from 'ionic-angular';

@Component({
  selector: 'page-model',
  templateUrl: 'model.html'
})
export class ModelPage {
  public photo:any;
  public photo_location:any;
  public state:any;
  public name:any;
  public lat:any;
  public lng:any;

  constructor(public navCtrl: NavController,private navParams: NavParams) {
    this.photo = navParams.get("photo");
    this.state = navParams.get("state");
    this.name = navParams.get("name");
    this.lat = navParams.get("lat");
    this.lng = navParams.get("lng");
  }
  closeModal() {
    this.navCtrl.pop();
  }
}
