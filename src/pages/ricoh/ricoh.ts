import { Component,Inject } from '@angular/core';
import { NavController } from 'ionic-angular';
import { RicohThetaService } from '../../app/services/ricohtheta.service';
import { RicohPicture } from '../../app/services/ricohpicture';

import { envVariables } from "../../app/environment-variables/environment-variables.token";

@Component({
  selector: 'page-ricoh',
  templateUrl: 'ricoh.html'
})
export class RicohPage {
  public photos : RicohPicture ;
  public callRicohService:boolean;
  constructor(public navCtrl: NavController,private ricohThetaService:RicohThetaService,@Inject(envVariables) public envVariables) {

  }
  ngOnInit(): void {
    this.callRicohService = this.envVariables.CALL_RICOH_SERVICES;
    if(this.callRicohService) {
      // This need to enabled for real RICOH THETA calling to get pictures
      this.photos = this.ricohThetaService.getPhotosList().subscribe(data => this.photos = data,
          err => alert('Problem with Calling EndPoint'));
    }else {
      this.photos = this.ricohThetaService.getPhotosListMock();
    }
  }

}
