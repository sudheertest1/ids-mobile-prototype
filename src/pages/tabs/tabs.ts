import { Component } from '@angular/core';

import { AboutPage } from '../about/about';
import { ContactPage } from '../contact/contact';
import { HomePage } from '../home/home';
import { MapPage } from '../map/map';
import { MapboxPage } from '../mapbox/mapbox';
import { RicohPage } from '../ricoh/ricoh';

@Component({
  templateUrl: 'tabs.html'
})
export class TabsPage {

  tab1Root = HomePage;
  tab2Root = AboutPage;
  tab3Root = ContactPage;
  tab4Root = MapPage;
  tab5Root = RicohPage;
  tab6Root = MapboxPage;
  constructor() {

  }
}
