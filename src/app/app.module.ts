import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { HTTP } from '@ionic-native/http';
import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { MapPage } from '../pages/map/map';
import { MapboxPage } from '../pages/mapbox/mapbox';
import { RicohPage } from '../pages/ricoh/ricoh';
import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { TabsPage } from '../pages/tabs/tabs';
import { ModelPage } from '../pages/model/model';

import { HttpModule, Http, XHRBackend, RequestOptions } from '@angular/http';
import { AuthenticatedHttpService } from "./authentication/authenticated-http.service";
import { EnvironmentsModule } from "./environment-variables/environment-variables.module";

import { RicohThetaService} from "./services/ricohtheta.service";


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    MapPage,
    RicohPage,
    AboutPage,
    ContactPage,
    TabsPage,
    ModelPage,
    MapboxPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    //LoginModule,
    HttpModule,
    EnvironmentsModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    MapPage,
    RicohPage,
    AboutPage,
    ContactPage,
    TabsPage,
    ModelPage,
    MapboxPage
    //MenuComponent
  ],
  providers: [
    StatusBar,
    SplashScreen,
    HTTP,
    RicohThetaService,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    {
      provide: AuthenticatedHttpService,
      useFactory: (backend: XHRBackend, defaultOptions: RequestOptions) => {
        return new AuthenticatedHttpService(backend, defaultOptions);
      },
      deps: [XHRBackend, RequestOptions]
    }
  ]
})
export class AppModule {}
