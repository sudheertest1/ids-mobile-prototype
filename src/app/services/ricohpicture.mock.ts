import { RicohPicture } from './ricohpicture';
export const RPICTURE: RicohPicture  = {name: 'camera.takePicture', state: "done",results: {
  "fileUrl": "https://theta360.com/en/gallery/img/user_16.jpg"
} } ;
export const RICOHSESSION: RicohPicture  = {name: 'camera.takePicture', state: "done",results: {
  "sessionId": "SID_0001",
  "timeout":180
} } ;
export const RPICTURELIST: RicohPicture={name: 'camera.takePicture', state: "done",results: {
  "entries": [
    {
      "name": "R0010017.MP4",
      "uri": "https://theta360.com/en/gallery/img/user_01.jpg",
      "size": 5135574,
      "dateTimeZone": "2015:07:10 11:05:18+09:00",
      "width": 1920,
      "height": 1080,
      "recordTime": 2
    },
    {
      "name": "R0010016.JPG",
      "uri": "https://theta360.com/en/gallery/img/user_08.jpg",
      "size": 4214389,
      "dateTimeZone": "2015:07:10 11:00:35+09:00",
      "width": 5376,
      "height": 2688
    },
    {
      "name": "R0010015.JPG",
      "uri": "https://theta360.com/en/gallery/img/user_14.jpg",
      "size": 4217265,
      "dateTimeZone": "2015:07:10 11:00:34+09:00",
      "width": 5376,
      "height": 2688
    }
  ]
}
}
