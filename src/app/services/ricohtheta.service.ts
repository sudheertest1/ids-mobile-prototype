import { Injectable,Inject } from '@angular/core';
import { Http, Response } from '@angular/http';
import { RicohPicture } from './ricohPicture';
import { RPICTURE,RICOHSESSION,RPICTURELIST } from './ricohpicture.mock';
import { envVariables } from "../../app/environment-variables/environment-variables.token";
import 'rxjs/add/operator/map';

@Injectable()
export class RicohThetaService {
  public startSessionParams:{};
  public takePictureParams:{};
  public pictureListParams:{};
  public ricohURL:string = this.envVariables.RICOH_CAMERA_URL;
  constructor (private http: Http,@Inject(envVariables) public envVariables ) {}

  //Use this method to get capture a photo in RICOH THETA Camera
  takePhoto(location:any,sessionId:string) : any {
      this.takePictureParams = {
           "name": "camera.takePicture",
            "parameters": {
                        "sessionId": sessionId,
                        "gpsInfo": {"lat": location.lat, "lng": location.lng}
                    }
                }

    return this.http.get(this.ricohURL,{ search: this.takePictureParams })
    .map((res:Response) => res.json(),
    (err:Error) => err);
  }

  //Comment this out if you are using original service
  takePhotoMock(location:any,sessionId:string) : RicohPicture {
    return RPICTURE;
  }

  //Use this method to get all the photos from RICOH THETA Service
  getPhotosList(): any {
     this.pictureListParams = {
           "name": "camera._listAll",
            "parameters": {
                        "entryCount": 3,
                    }
                }

    return this.http.post(this.ricohURL,this.pictureListParams)
    .map((res:Response) => res.json(),
    (err:Error) => err);
  }

  //Comment this out if you are using original service
  getPhotosListMock(): RicohPicture {
    return RPICTURELIST;
  }

  //Use this method for Opening a session for Ricoh Theta Camera
  startSession(): any {
      debugger;
    this.startSessionParams ={
        "name":"camera.startSession",
            "parameters": {}
    }
      var headers = {
          'Content-Type' : 'application/json; charset=utf-8',
          'X-XSRF-Protected': '1'
      };

    return this.http.post(this.ricohURL,this.startSessionParams,headers).map((res:Response) => res.json(),(err:Error) => err);
    //return RPICTURE;
  }

  //Comment this out if you are using original service
  startSessionMock(): RicohPicture {
    return RICOHSESSION;
  }


}
