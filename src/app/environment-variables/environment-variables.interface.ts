export interface EnvironmentVariables {
  readonly API_URL: string;
  readonly SERVER_URL: string;
  readonly OAUTH_REDIRECT_URL: string;
  readonly OAUTH_CLIENT_ID: string;
  readonly CALL_RICOH_SERVICES:boolean;
  readonly BING_KEY: string;
  readonly OPEN_STREET_MAP_URL: string;
  readonly RICOH_CAMERA_URL: string;
}