import { EnvironmentVariables } from './environment-variables.interface';

export const prodVariables: EnvironmentVariables = {
  API_URL: "http://qa-api.voyageframework.com/api/v1",
  SERVER_URL: "http://qa-api.voyageframework.com",
  OAUTH_REDIRECT_URL: "http://localhost:3000",
  OAUTH_CLIENT_ID: "client-super",
  CALL_RICOH_SERVICES : false,
  BING_KEY: "AuhiCJHlGzhg93IqUH_oCpl_-ZUrIE6SPftlyGYUvr9Amx5nzA-WqGcPquyFZl4L",
  OPEN_STREET_MAP_URL: "http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png",
  RICOH_CAMERA_URL : "http://192.168.1.1/osc/commands/execute"
};